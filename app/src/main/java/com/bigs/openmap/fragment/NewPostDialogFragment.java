package com.bigs.openmap.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bigs.openmap.R;
import com.bigs.openmap.activity.MapActivity;
import com.bigs.openmap.object.Post;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by bigs on 2016. 7. 24..
 */
public class NewPostDialogFragment extends DialogFragment {

    private AppCompatActivity parentActivity = null;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        parentActivity = (AppCompatActivity) getActivity();
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View rootView = inflater.inflate(R.layout.dialog_add_post, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(rootView)
                // Add action buttons
                .setPositiveButton("만들기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        String hostUrl = getString(R.string.host);
                        EditText etTitle = (EditText) rootView.findViewById(R.id.et_post_title);
                        EditText etDesc = (EditText) rootView.findViewById(R.id.et_post_desc);

                        if(etTitle.getText().toString().equals("")) {
                            Toast.makeText(parentActivity, "맵이름을 입력하세요.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(etDesc.getText().toString().equals("")) {
                            Toast.makeText(parentActivity, "맵정보를 입력하세요.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        AsyncHttpClient httpClient = new AsyncHttpClient();
                        JSONObject postObject = new JSONObject();
                        try {
                            postObject.put("category", "uncategorized");
                            postObject.put("title", etTitle.getText().toString());
                            postObject.put("desc", etDesc.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        StringEntity entity = null;
                        entity = new StringEntity(postObject.toString(),"utf-8");

                        httpClient.post(getContext(), hostUrl+"/posts/",
                                entity, "application/json",
                                new JsonHttpResponseHandler(){
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        Toast.makeText(parentActivity,"추가 성공", Toast.LENGTH_SHORT).show();

                                        Post post = new Post();
                                        try {
                                            post.pk = Integer.parseInt(response.getString("pk"));
                                            post.title = response.getString("title");
                                            post.desc = response.getString("desc");
                                            post.number_of_locations = Integer.parseInt(response.getString("number_of_location"));
                                            post.categoy = response.getString("category");
                                            post.creator = response.getString("creator");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        Intent intent = new Intent(parentActivity.getApplicationContext(), MapActivity.class);
                                        intent.putExtra("post", post);

                                        parentActivity.startActivity(intent);
//                                        NewPostDialogFragment.this.getDialog().cancel();

                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                        Toast.makeText(getActivity(),"추가 실패", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                        Toast.makeText(getActivity(),"추가 실패", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NewPostDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
