package com.bigs.openmap.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bigs.openmap.R;
import com.bigs.openmap.object.Post;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapLocationAddFragment extends Fragment {

    private GoogleMap map = null;

    private Post post = null;
    private AsyncHttpClient httpClient = new AsyncHttpClient();
    private String hostUrl = null;

    private EditText etTitle = null;
    private EditText etDesc = null;

    private AppCompatActivity parentActivity = null;

    public static MapLocationAddFragment newInstance(Post post) {
        MapLocationAddFragment f = new MapLocationAddFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("post", post);
        f.setArguments(args);

        return f;
    }

    public MapLocationAddFragment() {
        // Required empty public constructor
    }

    public void setMap(GoogleMap map){
        this.map = map;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        post = getArguments().getParcelable("post");
        View view = inflater.inflate(R.layout.fragment_map_location_add, container, false);
        parentActivity = (AppCompatActivity)getActivity();
        final ImageView ivLocationAddFinish =
                (ImageView) parentActivity.getSupportActionBar()
                        .getCustomView().findViewById(R.id.iv_add_location_finish);
        final ImageView ivBack =
                (ImageView) parentActivity.getSupportActionBar()
                        .getCustomView().findViewById(R.id.iv_back_to_map);

        etTitle = (EditText) view.findViewById(R.id.et_map_location_info_title);
        etDesc = (EditText) view.findViewById(R.id.et_map_location_info_desc);

        hostUrl = getString(R.string.host);

        ivLocationAddFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(map == null)
                    return;
                if(etTitle.getText().toString().equals("")) {
                    Toast.makeText(parentActivity, "위치이름을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(etDesc.getText().toString().equals("")) {
                    Toast.makeText(parentActivity, "위치정보을 입력해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }
                final double lat = map.getCameraPosition().target.latitude;
                final double lang = map.getCameraPosition().target.longitude;
                JSONObject locationObject = new JSONObject();
                try {
                    locationObject.put("lat",lat);
                    locationObject.put("lang",lang);
                    locationObject.put("title", etTitle.getText().toString());
                    locationObject.put("desc", etDesc.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                StringEntity entity = null;
                entity = new StringEntity(locationObject.toString(),"utf-8");

                Log.d("test", locationObject.toString());
                httpClient.post(getContext(), hostUrl+"/posts/"+post.pk+"/locations/",
                        entity, "application/json",
                        new JsonHttpResponseHandler(){
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                Toast.makeText(parentActivity,"추가 성공", Toast.LENGTH_SHORT).show();
                                Fragment fragment =
                                        parentActivity.getSupportFragmentManager()
                                                .findFragmentByTag("fmMapInfo");
                                if(fragment != null){
                                    TextView tvNOL = (TextView)fragment.getView().findViewById(R.id.tv_num_location);
                                    tvNOL.setText(String.valueOf(Integer.parseInt(tvNOL.getText().toString())+1));
                                }
                                Log.d("test", response.toString());
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                Toast.makeText(parentActivity,"추가 실패", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                Toast.makeText(parentActivity,"추가 실패", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFinish() {
                                map.addMarker(new MarkerOptions()
                                                .position(new LatLng(lat, lang))
                                                .title(etTitle.getText().toString())
                                                .snippet(etDesc.getText().toString()));
                                ivBack.performClick();
                            }
                        });
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
