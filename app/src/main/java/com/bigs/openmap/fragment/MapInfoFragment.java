package com.bigs.openmap.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bigs.openmap.R;
import com.bigs.openmap.object.Post;

import org.w3c.dom.Text;

public class MapInfoFragment extends Fragment {

    public static MapInfoFragment newInstance(Post post) {
        MapInfoFragment f = new MapInfoFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("post", post);
        f.setArguments(args);

        return f;
    }

    private Post post = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        post = getArguments().getParcelable("post");
        View view = inflater.inflate(R.layout.fragment_map_info, container, false);
        TextView tvDesc = (TextView)view.findViewById(R.id.tv_map_info_desc);
        TextView tvNOL = (TextView)view.findViewById(R.id.tv_num_location);
        tvDesc.setText(post.desc);
        tvNOL.setText(String.valueOf(post.number_of_locations));
        return view;
    }

}
