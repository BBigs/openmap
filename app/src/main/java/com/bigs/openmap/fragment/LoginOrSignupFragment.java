package com.bigs.openmap.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bigs.openmap.R;
import com.bigs.openmap.activity.LoginActivity;
import com.bigs.openmap.activity.MainActivity;
import com.bigs.openmap.object.Post;

/**
 * Created by bigs on 2016. 7. 24..
 */
public class LoginOrSignupFragment extends Fragment {

    public static LoginOrSignupFragment newInstance(MainActivity.MapListFragmentAdapter adapter) {
        LoginOrSignupFragment f = new LoginOrSignupFragment();
        return f;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login_or_signup, container, false);

        Button btLogin = (Button)rootView.findViewById(R.id.bt_login);
        Button btSignUp = (Button)rootView.findViewById(R.id.bt_signup);

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivityForResult(intent, 1);

            }
        });

        return rootView;
    }


}
