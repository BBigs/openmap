package com.bigs.openmap.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bigs.openmap.R;
import com.bigs.openmap.activity.MapActivity;
import com.bigs.openmap.object.Post;
import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.TextViewAction;
import com.dexafree.materialList.listeners.RecyclerItemClickListener;
import com.dexafree.materialList.view.MaterialListView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by bigs on 2016-07-15.
 */
public class MapListFragment extends Fragment {

    private LinearLayoutManager mLayoutManager = null;
    private SwipeRefreshLayoutBottom mLayout = null;
    private MaterialListView mListView = null;

    private HttpPostListHandler mHttpPostListHandler = new HttpPostListHandler();
    private AsyncHttpClient mHttpClient = new AsyncHttpClient();
    private String mHostUrl = null;

    public MapListFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mLayout = (SwipeRefreshLayoutBottom) inflater.inflate(R.layout.fragment_map_list, container, false);
        mListView = (MaterialListView) mLayout.findViewById(R.id.lv_maps);
        mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mListView.setLayoutManager(mLayoutManager);
        mListView.addOnItemTouchListener(new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull Card card, int position) {
                Intent intent = new Intent(getContext(), MapActivity.class);
                intent.putExtra("post", (Post)card.getTag());
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(@NonNull Card card, int position) {

            }
        });

        //http
        mHostUrl = getContext().getResources().getString(R.string.host);
        if (mHttpPostListHandler.hasNextPage){//만약 데이터가 하나도 없으면 에러
            mLayout.setRefreshing(true);
            mHttpClient.get(mHostUrl + "/posts/", mHttpPostListHandler);    //default list
//            Log.d(this.getClass().toString(), "onFresh default get");
        }
        mLayout.setOnRefreshListener(new SwipeRefreshLayoutBottom.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mHttpPostListHandler.hasNextPage) {
                    mHttpClient.get(mHttpPostListHandler.nextPageUrl, mHttpPostListHandler);
//                    Log.d(this.getClass().toString(), "onFresh get");
                }
                else
                    mLayout.setRefreshing(false);
            }
        });
        return mLayout;
    }




    private class HttpPostListHandler extends JsonHttpResponseHandler {
        public boolean hasNextPage = true;
        public String nextPageUrl = null;

        @Override
        public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
            try {
                nextPageUrl = response.getString("next");
//                Log.d(this.getClass().toString(), "nextPage : "+nextPageUrl == null? "null" : nextPageUrl);
                if (nextPageUrl.equals("null"))
                    hasNextPage = false;
                else
                    hasNextPage = true;
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Log.d(this.getClass().toString(), "nextPage : "+nextPageUrl);


//            postRunnable(new Runnable() {
//                @Override
//                public void run() {

                    int scrollIndex = mLayoutManager.findLastCompletelyVisibleItemPosition();


                    try {
                        JSONArray result = response.getJSONArray("results");
                        for (int i = 0; i < result.length(); i++) {
                            final Post post = new Post();
                            JSONObject postObject = result.getJSONObject(i);
                            post.title = postObject.getString("title");
                            post.desc = postObject.getString("desc");
                            post.number_of_locations = Integer.parseInt(postObject.getString("number_of_locations"));
                            post.pk = Integer.parseInt(postObject.getString("pk"));
                            Card card = new Card.Builder(getContext())
                                    .withProvider(new CardProvider())
                                    .setLayout(R.layout.card_map_item)
                                    .setTitle(post.title)
                                    .setDescription(post.desc)
                                    .addAction(R.id.tv_num_location, new TextViewAction(getContext())
                                            .setText(String.valueOf(post.number_of_locations))
                                            .setTextResourceColor(R.color.black_button))
                                    .addAction(R.id.tv_add_like_map, new TextViewAction(getContext())
                                            .setText(R.string.map_item_like)
                                            .setTextColor(Color.BLACK)
                                            .setListener(new OnActionClickListener() {
                                                @Override
                                                public void onActionClicked(View view, Card card) {
                                                    Toast.makeText(getContext(), "즐겨찾기", Toast.LENGTH_SHORT).show();
                                                }
                                            }))
                                    .endConfig()
                                    .build();
                            card.setTag(post);
                            mListView.getAdapter().add(card);
                            mListView.scrollToPosition(scrollIndex + 1);
                            mLayout.setRefreshing(false);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                }
//            });
        }


        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            Log.d("MainActivity", responseString);
        }

    }
}
