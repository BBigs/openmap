package com.bigs.openmap.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bigs.openmap.R;

import org.w3c.dom.Text;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapLocationInfoFragment extends Fragment {


    public MapLocationInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView tvTitle = (TextView)getView().findViewById(R.id.tv_map_location_info_title);
        TextView tvDesc = (TextView)getView().findViewById(R.id.tv_map_location_info_desc);
        tvTitle.setText(getArguments().getString("title"));
        tvDesc.setText(getArguments().getString("desc"));
    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        getArguments()
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map_location_info, container, false);
    }

}
