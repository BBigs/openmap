package com.bigs.openmap.object;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bigs on 2016. 7. 23..
 */
public class Post implements Parcelable{
    /*
            {
          "pk": "17",
          "creator": null,
          "category": "food",
          "number_of_locations": "15",
          "title": "1",
          "desc": "1"
        }
     */
    public int pk;
    public String creator;
    public String categoy;
    public int number_of_locations;
    public String title;
    public String desc;

    public Post(){}

    public Post(Parcel in){
        pk = in.readInt();
        creator = in.readString();
        categoy = in.readString();
        number_of_locations = in.readInt();
        title = in.readString();
        desc = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(pk);
        parcel.writeString(creator);
        parcel.writeString(categoy);
        parcel.writeInt(number_of_locations);
        parcel.writeString(title);
        parcel.writeString(desc);

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
}
