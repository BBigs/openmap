package com.bigs.openmap.activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigs.openmap.R;
import com.bigs.openmap.fragment.MapInfoFragment;
import com.bigs.openmap.fragment.MapLocationAddFragment;
import com.bigs.openmap.fragment.MapLocationInfoFragment;
import com.bigs.openmap.object.Post;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private View overlayMap = null;
    private MapInfoFragment fmMapInfo = null;
    private MapLocationInfoFragment fmLocationInfo = null;
    private MapLocationAddFragment fmLocationAdd = null;
    private View actionBarMapInfo = null;
    private View actionBarMapLocationAdd = null;
    private ImageView ivBack2 = null;
    private GoogleMap map = null;

    private String hostUrl = null;
    private AsyncHttpClient httpClient = new AsyncHttpClient();

    private Post post = new Post();
    private Marker clikedMarker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        post = (Post)getIntent().getParcelableExtra("post");
        hostUrl = getString(R.string.host);

        actionBarMapInfo = getLayoutInflater().inflate(R.layout.actionbar_map_info, null);
        actionBarMapLocationAdd = getLayoutInflater().inflate(R.layout.actionbar_map_location_add, null);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setCustomView(actionBarMapInfo);

        ((TextView)actionBar.getCustomView().findViewById(R.id.tv_map_info_title)).setText(post.title);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ImageView ivBack = (ImageView) actionBarMapInfo.findViewById(R.id.iv_back_to_map_list);
        ImageView ivAddLocation = (ImageView) actionBarMapInfo.findViewById(R.id.iv_add_location);
        overlayMap = (View) findViewById(R.id.layout_cross_line);
        fmLocationAdd = MapLocationAddFragment.newInstance(post);
        fmLocationInfo = new MapLocationInfoFragment();
        fmMapInfo = MapInfoFragment.newInstance(post);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_map_info_wrapper, fmMapInfo, "fmMapInfo");
        ft.commit();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ivAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //change
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fragment_map_info_wrapper, fmLocationAdd);
                ft.commit();

                ActionBar actionBar = getSupportActionBar();
                actionBar.setCustomView(actionBarMapLocationAdd);

                MapActivity.this.overlayMap.setVisibility(View.VISIBLE);
                if(clikedMarker != null)
                    clikedMarker.hideInfoWindow();

            }
        });

        ivBack2 = (ImageView) actionBarMapLocationAdd.findViewById(R.id.iv_back_to_map);
        ImageView ivAddLocationFinish = (ImageView) actionBarMapLocationAdd.findViewById(R.id.iv_add_location_finish);

        ivBack2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.remove(fmLocationAdd);
                ft.commit();
                ActionBar actionBar = getSupportActionBar();
                actionBar.setCustomView(actionBarMapInfo);
                MapActivity.this.overlayMap.setVisibility(View.INVISIBLE);
            }
        });

        this.overlayMap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });


    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getFragments().contains(fmLocationAdd)){
            ivBack2.performClick();
        }else {
            super.onBackPressed();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("MapActivity", "onMapReady");
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.zoomTo(10));

        fmLocationAdd.setMap(map);
        httpClient.get(hostUrl + "/posts/" + post.pk + "/locations/", new LocationListHandler());
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                FragmentManager fm = getSupportFragmentManager();
                if(fm.getFragments().contains(fmLocationInfo)) {
                    fmLocationInfo.getArguments().putString("title", marker.getTitle());
                    fmLocationInfo.getArguments().putString("desc", marker.getSnippet());
//                    fmLocationInfo.onStart();
                }else{
                    Bundle bundle = new Bundle();
                    bundle.putString("title", marker.getTitle());
                    bundle.putString("desc", marker.getSnippet());
                    fmLocationInfo.setArguments(bundle);
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.add(R.id.fragment_map_info_wrapper, fmLocationInfo);
                    ft.commit();

                }


            }
        });
        map.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
            @Override
            public void onInfoWindowClose(Marker marker) {
                FragmentManager fm = getSupportFragmentManager();
                if(fm.getFragments().contains(fmLocationInfo)) {
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.remove(fmLocationInfo);
                    ft.commit();
                }
            }
        });
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                clikedMarker = marker;
                return false;
            }
        });
    }

    //가져와서 넘기면 힘든게 뭐냐면, 가져왔을때 아직 프로그래먼트가 완성이 안됐을 수도 즉 해당 view에서 쓰려면 거기서 통신을 하는게?
//    private class PostHandler extends JsonHttpResponseHandler{
//        @Override
//        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
////            TextView tvDesc = (TextView) fmLocationInfo.getView().findViewById(R.id.tv_map_info_desc);
////            TextView tvNumOfLocations = (TextView) fmLocationInfo.getView().findViewById(R.id.tv_num_location);
//            TextView tvTitle = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.tv_map_info_title);
//            try {
//                tvTitle.setText(response.getString("title"));
////                tvDesc.setText(response.getString("desc"));
////                tvNumOfLocations.setText(response.getString("number_of_locations"));
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    }
    private class LocationListHandler extends JsonHttpResponseHandler {
        private boolean isCameraPositioned = false;
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            String nextPageUrl = null;

            try {
                nextPageUrl = response.getString("next");
                JSONArray results = response.getJSONArray("results");
                if(results.length() == 0){
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                           37.5607653581599,
                           126.980356313288),10));//seoul

                }
                for (int i = 0; i < results.length(); i++) {
                    final JSONObject location = results.getJSONObject(i);
                    if(!isCameraPositioned){
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                Double.parseDouble(location.getString("lat")),
                                Double.parseDouble(location.getString("lang"))),10));
                        isCameraPositioned = true;
                    }
                    postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("MapActivity", "add Marker");
                            try {
                                map.addMarker(new MarkerOptions()
                                        .position(new LatLng(
                                                Double.parseDouble(location.getString("lat")),
                                                Double.parseDouble(location.getString("lang"))))
                                        .title(location.getString("title"))
                                        .snippet(location.getString("desc"))
                                        );

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!nextPageUrl.equals("null")) {
                Log.d("MapActivity", "load more data - np:" + nextPageUrl);
                httpClient.get(nextPageUrl, new LocationListHandler());
            }
        }

    }
}
