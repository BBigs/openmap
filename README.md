Software Maestro 7기 과정 - 김재은

1차 과제 android source

[Google Play Link](https://play.google.com/store/apps/details?id=com.bigs.openmap)

# 과제 요건 #

만족시키지 못한 사항 <>로 표시

**-필수사항**

앱(게임)과 서버 통시하는 로직이 포함되어야함.

클라이언트 게임은 게임첫 설치 시간. 접속시간. 접속후 게임종료시간. 마지막 플레이시간등이 DB에 저장되어야 함.

<페이스북, 트위터, 이메일등 타 서비스 연계가 1개이상 되어야 함.>

앱스토어에 업로드 되어야함.

서버 프로그램이 클라우드나 호스팅이 되는 서버에 배포가 되어야함.

데이터는 DB에 저장되어 SQL을 통해서 조회 및 추가가 가능해야함.

백엔드는 BaaS나 클라우드 제한 없으나 웹서비스 형태이어야 함.


**-가점 사항**

클라우드 이용시 가점.

친구추가, 랭킹, 선물하기등의 기능 추가시 가점.

Restful API 형태 설계 적용시 가점.

<push 구현 되었으면 가점.>

<안드로이드와 아이폰 둘다 대응시 가점.>

DB와 연동되는 별도의 CMS(관리시스템)을 구축시 가점.





### 서버 구성 ###
The outside world <-> Nginx <-> The socket <-> Gunicorn <-> Django( + Django rest framework)

인증에 JWT Web Token 사용
AWS의 ec2, rds(mariaDB) 사용

## url 구성 ##
    ^users/$  : post-회원가입

    ^posts/(?P<pk>[0-9]+)/$ : get-특정 포스트정보 가져오기, patch-포스트 즐겨찾기

    ^posts/$ : get-포스트목록 가져오기, post-새로운 포스트 작성

    ^posts/(?P<post_id>[0-9]+)/locations/$ : get-포스트에 등록된 위치정보 가져오기 post-포스트에 위치정보 등록하기

    ^posts/(?P<post_id>[0-9]+)/locations/(?P<pk>[0-9]+)/$ : get-특정위치정보 가져오기

    ^like-posts/$ : 앞서 posts/와 같으나 현재 인가받은 유저의 즐겨찾기 목록만을 처리

    ^admin/ : CMS

    ^api-token-auth/ : post-유저정보를 통해 토큰 발급

## CMS 

[관리 패널](http://52.78.89.120/admin/)
![스크린샷 2016-07-24 오후 10.26.13.png](https://bitbucket.org/repo/oBgKdM/images/3614402266-%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA%202016-07-24%20%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE%2010.26.13.png)

admin 계정은 과제제출제메일로 첨부



##TODO
클라이언트 쪽에서 로그인, 회원가입 처리가 되지 않음(모두 guest로 처리됨. guest는 즐겨찾기 불가)
facebook로그인 예정